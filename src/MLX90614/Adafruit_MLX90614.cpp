/***************************************************
  This is a library for the MLX90614 Temp Sensor
****************************************************/

#include "Adafruit_MLX90614.h"

Adafruit_MLX90614::Adafruit_MLX90614(uint8_t i2caddr) { _addr = i2caddr; }

boolean Adafruit_MLX90614::begin(void) {
    wireBegin(1);
  /*
  for (uint8_t i=0; i<0x20; i++) {
    std::cout << i ;
    std::cout << "=" << '\n';
    std::cout << read16(i),HEX ;
  }
  */
  return true;
}

//////////////////////////////////////////////////////

double Adafruit_MLX90614::readObjectTempF(void) {
  return (readTemp(MLX90614_TOBJ1) * 9 / 5) + 32;
}

double Adafruit_MLX90614::readAmbientTempF(void) {
  return (readTemp(MLX90614_TA) * 9 / 5) + 32;
}

double Adafruit_MLX90614::readObjectTempC(void) {
  return readTemp(MLX90614_TOBJ1);
}

double Adafruit_MLX90614::readAmbientTempC(void) {
  return readTemp(MLX90614_TA);
}

float Adafruit_MLX90614::readTemp(uint8_t reg) {
  float temp;

  temp = read16(reg);
  temp *= .02;
  temp -= 273.15;
  return temp;
}

/*********************************************************************/

uint16_t Adafruit_MLX90614::read16(uint8_t a) {
  uint16_t ret;


  wireBeginTransmission(_addr); // start transmission to device
  wireWrite(a);                 // sends register address to read from
  wireEndTransmission(false);   // end transmission

  wireRequestFrom(_addr, (size_t)3); // send data n-bytes read
  ret = wireRead();                  // receive DATA
  ret |= wireRead() << 8;            // receive DATA

  uint8_t pec = wireRead();

  return ret;
}
