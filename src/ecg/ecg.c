#include <stdio.h>
#include <stdint.h>
#include <shunyaInterfaces.h>
#include <functions.h>
#include <pcf8591.h>
#include "ecg.h"
#include "log.h"

// USE : Mini PCF8591 AD DA Shell Module V2.0 only

static struct pcf8591Settings adc1 = {.def=1, .addr=0x48};
static struct pcf8591Settings adc2 = {.def=1, .addr=0x49};

//Funtion to check if the electrodes are connected
static int8_t check_electrode_connections(int pin1, int pin2)
{
	if(digitalRead(pin1) == 1 || digitalRead(pin2) == 1)
	{
		return -1;
	}
	return 0;
}

//Function for Lead I
int getLeadI()
{	
	if(check_electrode_connections(LO_POS1,LO_NEG1) < 0) {
		writeToLog("Electrodes not connected for Lead I");
		return -1;
	} else {
		return getAdc6(adc1,adc1_Channel0); 
	}
}

//Function for Lead II
int getLeadII()
{	
	if(check_electrode_connections(LO_POS2,LO_NEG2) < 0) {
		writeToLog("Electrodes not connected for Lead II");
		return -1;
	} else {
		return getAdc6(adc1,adc1_Channel1); 
	}
}

//Function for Lead III
int getLeadIII()
{
	if(check_electrode_connections(LO_POS3,LO_NEG3) < 0) {
		writeToLog("Electrodes not connected for Lead III");
		return -1;
	} else {
		return getAdc6(adc1,adc1_Channel2); 
	}

}

//Function for Lead aVR
int getLeadaVR()
{
	if(check_electrode_connections(LO_POS4,LO_NEG4) < 0) {
		writeToLog("Electrodes not connected for Lead aVR");
		return -1;
	} else {
		return getAdc6(adc1,adc1_Channel3); 
	}
}

//Function for Lead aVL
int getLeadaVL()
{
	if(check_electrode_connections(LO_POS5,LO_NEG5) < 0) {
		writeToLog("Electrodes not connected for Lead aVL");
		return -1;
	} else {
		return getAdc6(adc2,adc2_Channel1); 
	}
}

//Function for aVF
int getLeadaVF()
{
	if(check_electrode_connections(LO_POS6,LO_NEG6) < 0) {
		writeToLog("Electrodes not connected for Lead aVF");
		return -1;
	} else {
		return getAdc6(adc2,adc2_Channel2); 
	}
}

//Function for chest lead
int getChestLeads()
{	
	if(check_electrode_connections(LO_POS7,LO_NEG7) < 0) {
		writeToLog("Electrodes not connected for Lead V");
		return -1;
	} else {
		return getAdc6(adc2,adc2_Channel3); 
	}
}


//Function to configure pins as input 
void ecgInit()
{
	writeToLog("ECG Init Start");
	pinMode(LO_POS1,INPUT);
	pinMode(LO_NEG1,INPUT);
	
	pinMode(LO_POS2,INPUT);
	pinMode(LO_NEG2,INPUT);

	pinMode(LO_POS3,INPUT);
	pinMode(LO_NEG3,INPUT);

	pinMode(LO_POS4,INPUT);
	pinMode(LO_NEG4,INPUT);

	pinMode(LO_POS5,INPUT);
	pinMode(LO_NEG5,INPUT);

	pinMode(LO_POS6,INPUT);
	pinMode(LO_NEG6,INPUT);

	pinMode(LO_POS7,INPUT);
	pinMode(LO_NEG7,INPUT);
	writeToLog("ECG Init End");
}
