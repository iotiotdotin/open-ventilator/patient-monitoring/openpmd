#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <shunyaInterfaces.h>
#include <stdbool.h>


//Declaring the pin for trigger 	
const int triggerPin = 38;


int main()
{	
	
	pid_t pid = 0;
	pid_t sid = 0;

	//Creating Child process
	pid = fork();

 	if(pid < 0){
        	printf("fork failed!\n");
        	exit(1);
    	}

	// Parent process
    	if(pid > 0){
        	exit(0); //terminate the parent process successfully
	}
	
	//Change file mask
	umask(0);
	
	//set new session
	sid = setsid();

	if(sid < 0){
		exit(1);
	}

	// Change the current working directory to root.
	chdir("/");

	// Close stdin. stdout and stderr
	close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);

	while(true){
		
		
		//Initailizing the resetTrigger flag to false
		bool resetTrigger = false;
		
		
		//Configuring triggerPin and as Output Pin
		pinMode(triggerPin,OUTPUT);
		

		//Checking the triggerPin, to set  resetTrigger to true
		if(digitalRead(triggerPin)){
			resetTrigger = true;
		}

		//Reset if resetTrigger is true
		if(resetTrigger == true){
			system("reboot");
		}
	}
	
	return 0;
}

