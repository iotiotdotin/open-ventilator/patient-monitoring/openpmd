**The AFE440 example file has the following fucntions:**

1.void afe44xxWrite (uint8_t address, uint32_t data) 

    Address and data are passed to this fuction and it returns void
    
    In this function the Chip select is made LOW
    
    First the addres is sent to the device
    
    Then the data is written and the chip select is made HIGH
    
2. uint32_t afe44xxRead (uint8_t address);

    Chip select is made low.
    
    Address is dent to the device
    
    The data is read 8 bits at a time
    
    Address of type uint8_t is passed as argument and it returns uint32_t datatype
    
3.void afe44xx_drdy_event()

    This fucntion when called makes the drdy_trigger HIGH. It takes no parameters and returns void
    
4. void afe44xxInit (void)

    This funtion is used to initialize AFE440 module
    
    This function will write commands to initialize LED's and ADC's
    
5. void estimate_spo2(uint16_t *pun_ir_buffer, int32_t n_ir_buffer_length, uint16_t *pun_red_buffer,
   int32_t *pn_spo2, int8_t *pch_spo2_valid, int32_t *pn_heart_rate, int8_t *pch_hr_valid)
   
    Returns void and takes address of pun_ir_buffer, n_ir_buffer_length , address of pun_red_buffer, address of pn_spo2,
    address of pch_spo2_valid, address of pn_heart_rate and address of pch_hr_valid as arguments
    
    Calulates the DC mean using a for loop
    
    Removes DC and invert signal so that we can use peak detector as valley detector
    
    Calculates threshold 
    
    find max between two valley locations
    
6. void find_peak( int32_t *pn_locs, int32_t *n_npks,  int32_t  *pn_x, int32_t n_size, int32_t n_min_height,
   int32_t n_min_distance, int32_t n_max_num )
   
   This fucntion finds peaks
   
 7. void find_peak_above( int32_t *pn_locs, int32_t *n_npks,  int32_t  *pn_x, int32_t n_size, int32_t n_min_height ) 
    
     Find peaks above n_min_height
     
8. void remove_close_peaks(int32_t *pn_locs, int32_t *pn_npks, int32_t *pn_x, int32_t n_min_distance)
     
     Remove peaks separated by less than MIN_DISTANCE
     
9. void sort_ascend(int32_t  *pn_x, int32_t n_size) 
     
     Sorts the array in asccending order
     
10.void sort_indices_descend(  int32_t  *pn_x, int32_t *pn_indx, int32_t n_size)

     Returns void 
     
     Takes arguments as peak index address, 
     
     Sort indices according to descending order (insertion sort algorithm)
   