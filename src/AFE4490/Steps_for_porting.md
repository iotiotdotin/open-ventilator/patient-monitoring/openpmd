## Some common steps of porting include:

* Serial.print was replaced ny printf
* Shunya SPI functions were used instead of arduino SPI functions
* Shunya interfaces library was included
* Boolean was replaced by bool since c supports bool only
* stdbool.h library was included to make use of boolean keywords
* stdint.h library was included to make use of uint8_t and similar variable types

## Problems faced:

* spiBegin() : Error asking for arguments to be passed