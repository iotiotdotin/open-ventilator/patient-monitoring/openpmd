# Patient Monitoring Device (Community Project)

![openpmd](https://uploads.ifdesign.de/award_img_310/oex_large/191876_01_310-1-191876_Patien_Monitoring_System_1.jpg)

Image credits: To original author and ifdesign.de site.

Easy to assemble and just enough Patient Monitoring Device for monitoring COVID-19 patients. 
The aim was to make a just enough but reliable Patient Monitoring Device uisng commonly available components.


This is not a professional project, but a project taken up by the IoTIoT.in Community.
If you would like to convert this to a professional project/product contact support@shunyaos.org.


## Shunya Stack 
Project is built by using the Shunya stack.

1.  ShunyaOS is a lightweight Operating system with built-in support for AI and IOT.
2.  ShunyaInterfaces Low Code platform to build AI, IoT and AIoT products, that allows you to rapidly create and deploy AIoT products with ease.

For more information on ShunyaOS see : http://demo.shunyaos.org 

## Installation 
To build your own PMD, See [Instructions](https://gitlab.com/iotiotdotin/open-ventilator/patient-monitoring/openpmd/-/wikis/Installation)

## Documentation 

For develeopers see detailed Documentation on the components of the project in the [Wiki](https://gitlab.com/iotiotdotin/open-ventilator/patient-monitoring/openpmd/-/wikis/home)

## Project Overview 

1.  [Project Plan Excel](https://docs.google.com/spreadsheets/d/1xS5S-x9y1HXhPFJlyepfs74IbRn8zmiqJ6g163G8PF8/edit#gid=2071887808)
2.  [Get a birds-eye status of the project](https://gitlab.com/iotiotdotin/open-ventilator/patient-monitoring/openpmd/-/milestones)

## Contributing 

Help us improve the project.

**Ways you can help**:

1.  Choose from the existing issue and work on those issues.
1.  Feel like the project could use a new feature, make an issue to discuss how it can be implemented and work on it.
1.  Find a bug create an Issue and report it.
1.  Review Issues or Merge Requests, give the develeopers the feedback.
1.  Fix Documentation.


## Contributers 

1.  Vaibhav Shinde
1.  Pranali Gadhave 
1.  Chanchal Choudhary
1.  Manjeet Singh 

